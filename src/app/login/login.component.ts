import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import { LoginService } from './login.service';
import { error } from '@angular/compiler/src/util';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  headers:any;
  loginForm: FormGroup;
  invalidLogin: boolean = false;
  constructor(private formBuilder: FormBuilder, private loginService: LoginService) { }

  ngOnInit() {
    // window.localStorage.removeItem('token');
    this.loginForm = this.formBuilder.group({
      mobile: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]
    });
    console.log("Data Info ",this.loginForm);
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    const loginPayload = {
      mobile: this.loginForm.controls.mobile.value,
      password: this.loginForm.controls.password.value,
      forceLogin:true

   
    }
    

    
    this.loginService.login(loginPayload).subscribe((res ) => {

    // readonly headers: Headers;
    // console.log("res:Response",res.headers );
    console.log("res:Response",res );
      

      //console.log("login Response",data);
      console.log("Response ",res);
      // if(Resp.status === 200) {
      //   console.log("Data value ",JSON.stringify(Resp));
      //   //window.localStorage.setItem('token', data.result.token);
      //   //this.router.navigate(['list-user']);
      //   alert("Success");

      // }else {
      //   this.invalidLogin = true;
      //   alert(Resp.message);
      // }
    
      
    },error=>{
      this.invalidLogin = true;
    }
    );

  }



}
