import { Injectable } from '@angular/core';
import {Observable, from} from "rxjs/index";

import { HttpClient, HttpResponse } from '@angular/common/http';
import { LoginResponse } from '../model/login.responce';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //baseUrl: string = 'http://localhost:5000/';

  constructor(private http: HttpClient) {}

   login(loginPayload){
     
    return this.http.put('http://localhost:5000/tokens', loginPayload)
    .pipe(map(reponse => reponse))
   
  }

// getUsers() : Observable<ApiResponse> {
//   return this.http.get<ApiResponse>(this.baseUrl);
// }

  }

